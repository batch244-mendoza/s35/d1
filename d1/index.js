const express = require("express");
// importing mongoose library
const mongoose = require("mongoose");

const app = express();
const port = 3001;
// [Section] MongoDB connection

// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
// Due to updates in Mongo DB drivers that allow connection to it, the default connection string is being flagged as an error
// By default a warning will be displayed in the terminal when the application is run, but this will not prevent Mongoose from being used in the application
// { newUrlParser : true } allows us to avoid any current and future errors while connecting to Mongo DB

// Syntax
	// mongoose.connect("<MongoDB Atlas connection string>", { useNewUrlParser : true });

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.a1svtt9.mongodb.net/b244-to-do?retryWrites=true&w=majority",
		{
			useNewUrlParser : true,
			useUnifiedTopology : true
		}
	);
// sets notifications for connection success or failure
// works with on and once Mongoose methods
let db = mongoose.connection;
// if a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));

// if the connection is successful, output the message in the console
db.once("open", ()=>{console.log("We're connected to the database")});
// [Section] Mongoose Schema
// Schema determines the structure of the documents to be written in the database
const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
});

// [Section] Model
// uses schema and are used to created/instantiate objects that correspond to the schema
// Server > Schema (blueprints) > Database > Collection
/*
	MVC naming Convention
		capitalized
		singular form
*/
/*
	the "Task" argument is the collection that will be created where the validated data will be stored.

	the second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
*/
const Task = mongoose.model("Task", taskSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Business Logic
/*
Creating a new task
	1. add a functionality to check if there are duplicate tasks ind the db.
		- if there is a duplicate, return an error
		- if there is no duplicate, add it in the database
	2. the task data will be coming from the request body
	3. create a new Task object with a "name" property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res) => {
	// findOne() returns the first document that matches the search criteria
	Task.findOne({name : req.body.name}, (err, result) => {
		// if a document was found and the document's name matches the information sent via the Postman (req.body)
		if(result != null && result.name == req.body.name){
			// return a message to the client/Postman as response
			return res.send("Duplicate task found");
		// if no document was found
		}else{
			// create a new Task object and save it in the db
			let newTask = new Task({
				name : req.body.name
			});
			// save() will store the information to the database
			newTask.save((saveErr, savedTask) => {
				// if error is encountered during saving
				if(saveErr){
					// return an error in the console
					return console.error(saveErr);
				// if there are no errors found
				}else{
					// return a status code pf 201 for created
					// send a message "new task created"
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

/*
Miniactivity
	write codes for getting all tasks in our database (get method , "/tasks" endpoint)

	Business Logic:
		1. Retrieve all documents (find method)
		2. if an error is encoutered, print the error
		3. if no errors are found, send a success status (200) back to the client and return an array of documents
10 minutes: solution discussion at 9:00 pm PST you can send Postman client outputs in the Google chat
*/
app.get("/tasks", (req, res) => {
	// find() is similar to "find" in MongoDB and an empty {} means that it returns all the documents and stores them in the "result" parameter
	Task.find({}, (err, result) => {
		// if an error occured
		if (err) {
			// print the error in the console
			return console.error(err);
		}
		// if no errors occured
		else{
			// send a 200 success status back to client and return an array of documents
			// json method allows to send a JSON format for the response
			return res.status(200).json({data:result})
		}
	})
})

// ACTIVITY S35----------------------------------------------------------------
//  Create a User schema.
const userSchema =new mongoose.Schema({
	username : String,
	password: String
})

//  Create a User model.
const User = mongoose.model("User", userSchema);

//  Create a POST route that will access the "/signup" route that will create a user.
app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if (result !== null && result.username == req.body.username){
			return res.send("Duplicate username is found");
		} else {
			if (req.body.username !== ''&& req.body.password !== ''){
				let newUser = new User ({
					username : req.body.username,
					password : req.body.password
				});

				newUser.save((saveErr, savedTask) => {
					if(saveErr){
						return console.error(saveErr);
					} else {
						return res.status(201).send("New user registered")
					}
				})
			} else {
				return res.send("Both username and password must be provided.!")
			}
		}
	})
})


app.listen(port, () => {console.log(`Server running at port ${port}`)});
