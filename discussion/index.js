const express = require ("express");
// importing mongoose library
const mongoose = require("mongoose");

const app = express();

const port = 3001;

mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.a1svtt9.mongodb.net/b244-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	});

// sets notifications for conneection success or failure
// works well with on and once Mongoose methods 
let db = mongoose.connection;

// if a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal 
db.on("error", console.error.bind(console, "connection error"));

// if the connection is successful, output the message in the console
db.once("open", () => {console.log("We're connected to the database")});


// [SECTION] Mongoose Schema
// Schema determines the structure of the documents to be written in the database 
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default : "pending"
	}
});

// [SECTION] Model
// uses schema and are used to created/instantiate objects that correspon to the schema
// Server > Schema (blueprints) > Database > Collection


/*

	MVC naming Convention
		captitalized 
		singular form
*/


/*
	the "Task" argument is the collection that will be created where the validated data will be stored.

	the second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection

*/

 const Task = mongoose.model("Task", taskSchema);



app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Business.Logic

/*

	Creating a new task
		1. add a functionality to check if there are duplicate tasks ind the db. 
			- if there is a duplicate , return an error
			- if there is no duplicate , add it in the database

		2. the task data will be coming from the request body
		3. create a new Task object with a "name" property
		4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object.


*/

app.post("/tasks", (req, res) =>{

	// findOne() returns the first document that matches the search criteria
	Task.findOne({name : req.body.name}, (err, result) =>{
		// if a document was found and the document's name matches the information sent via the Postman (req.body)
		if(result != null && result.name == req.body.name){
			// return a message to the client/Postman as response
			return res.send("Duplicate task found");
			// if no document was found
		}else{
			// create a new Task object and save it in the db
			let newTask = new Task({
				name : req.body.name
			});
			// save() will store the information to the database 
			newTask.save((saveErr, savedTask) => {
				// if error is encountered during saving
				if(saveErr){
					// return an error in the console
					return console.error(saveErr);
					// if there are no errors found
				}else{
					// return a status code pf 201 for created
					// send a message "new task created"
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

/*
	Miniactivity
		write codes for getting all tasks in our database (get method)

		Business Logic: 
			1. Retrieve all documents (find method)
			2. if an error is encountered, print the error
			3. if no errors are found, send a success status (200) back to the client and return an array of documents
			

*/

app.listen(port, () => {console.log(`Server is running at port ${port}`)});